import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import * as bcrypt from 'bcrypt';

// const saltOrRounds = 10;

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private emplyeesRepository: Repository<Employee>,
  ) {}
  async create(createEmployeeDto: CreateEmployeeDto) {
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createEmployeeDto.password, salt);
    createEmployeeDto.password = hash;
    return this.emplyeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.emplyeesRepository.find();
  }

  async findOne(id: number) {
    const employee = await this.emplyeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async findOneByUsername(username: string) {
    const employee = await this.emplyeesRepository.findOne({
      where: { username: username },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    if (updateEmployeeDto.password !== undefined) {
      const salt = await bcrypt.genSalt();
      const hash = await bcrypt.hash(updateEmployeeDto.password, salt);
      updateEmployeeDto.password = hash;
    }
    const employee = await this.emplyeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    return this.emplyeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const employee = await this.emplyeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.emplyeesRepository.remove(employee);
  }
}
