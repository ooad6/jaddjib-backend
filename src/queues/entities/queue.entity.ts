import { Employee } from 'src/employees/entities/employee.entity';
import { OrderItem } from 'src/orders/entities/order-items.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Queue {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Table, (table) => table.queue)
  table: Table;

  @Column()
  name: string;

  @Column()
  status: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => OrderItem, (orderItems) => orderItems.queue)
  orderItems: OrderItem;

  @ManyToOne(() => Employee, (employee) => employee.queue)
  employee: Employee;
}
